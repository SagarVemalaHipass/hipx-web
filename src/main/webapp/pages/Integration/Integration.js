Application.$controller("IntegrationPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.integrationListTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };


    $scope.integrationListTableDatarender = function($isolateScope, $data) {
        var totalRecords = $scope.Widgets.integrationListTable.dataNavigator.dataSize;
        var maxResults = $scope.Widgets.integrationListTable.dataNavigator.maxResults;
        var currentPage = $scope.Widgets.integrationListTable.dataNavigator.dn.currentPage;

        $scope.paginationPrefix = $scope.$root.getPaginationPrefix(totalRecords, maxResults, currentPage);
    };

}]);


Application.$controller("integrationListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvCreateIntegration.dataSet = $rowData;
            $scope.Variables.stvEditIntegration.dataSet = $rowData;
        };


        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditIntegration.dataSet = {};

        };

    }
]);

Application.$controller("integrationDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.saveButtonClick = function($event, $isolateScope) {
            $scope.Widgets.createIntegrationForm.submit();
        };


        $scope.createIntegrationFormSubmit = function($event, $isolateScope, $formData) {
            //Create Integration
            if ($scope.Variables.stvEditIntegration.dataSet.integrationId === undefined) {
                $scope.Variables.createIntegration.setInput('RequestBody', $scope.Variables.stvCreateIntegration.dataSet);
                $scope.Variables.createIntegration.invoke();
            } else {
                //Update Integration
                $scope.Variables.editIntegration.setInput('RequestBody', $scope.Variables.stvCreateIntegration.dataSet);
                $scope.Variables.editIntegration.invoke();
            }

        };

    }
]);