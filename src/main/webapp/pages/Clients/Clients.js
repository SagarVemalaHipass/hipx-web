Application.$controller("ClientsPageController", ["$scope", function($scope) {
    "use strict";
    $scope.paginationPrefix = "";
    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */

    };


    $scope.clientsListTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };


    $scope.textPaginationSizeChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Variables.clientList1.update();
    };


    $scope.clientsListTableDatarender = function($isolateScope, $data) {
        var totalRecords = $scope.Widgets.clientsListTable.dataNavigator.dataSize;
        var maxResults = $scope.Widgets.clientsListTable.dataNavigator.maxResults;
        var currentPage = $scope.Widgets.clientsListTable.dataNavigator.dn.currentPage;

        $scope.paginationPrefix = $scope.$root.getPaginationPrefix(totalRecords, maxResults, currentPage);
    };

}]);


Application.$controller("clientsListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;


        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditClient.dataSet = {};
        };


        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvCreateClient.dataSet = $rowData;
            $scope.Variables.stvEditClient.dataSet = $rowData;
        };

    }
]);

Application.$controller("manageClientDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.createClientFormSubmit = function($event, $isolateScope, $formData) {
            //Create Client
            if ($scope.Variables.stvEditClient.dataSet.id === undefined || $scope.Variables.stvEditClient.dataSet.id === "" || $scope.Variables.stvEditClient.dataSet.id === null) {
                $scope.Variables.createClient.setInput('RequestBody', $scope.Variables.stvCreateClient.dataSet);
                $scope.Variables.createClient.invoke();
            } else {
                //Update Client
                $scope.Variables.editClient.setInput('RequestBody', $scope.Variables.stvCreateClient.dataSet);
                $scope.Variables.editClient.invoke();
            }
        };


        $scope.saveButtonClick = function($event, $isolateScope) {
            $scope.Widgets.createClientForm.submit();

        };

    }
]);