Application.$controller("MasterSettingsConfigurationsPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.configListTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };


    $scope.configListTableDatarender = function($isolateScope, $data) {
        var totalRecords = $scope.Widgets.configListTable.dataNavigator.dataSize;
        var maxResults = $scope.Widgets.configListTable.dataNavigator.maxResults;
        var currentPage = $scope.Widgets.configListTable.dataNavigator.dn.currentPage;

        $scope.paginationPrefix = $scope.$root.getPaginationPrefix(totalRecords, maxResults, currentPage);
    };

}]);


Application.$controller("configListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;


        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvEditConfig.dataSet = $rowData;
            $scope.Variables.stvConfig.dataSet = $rowData;
        };


        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditConfig.dataSet = {}
        };

    }
]);

Application.$controller("dialogManageConfigSettingsController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.buttonSaveClick = function($event, $isolateScope) {
            $scope.Widgets.createConfigForm.submit();
        };


        $scope.createConfigFormSubmit = function($event, $isolateScope, $formData) {
            // Create Config
            if ($scope.Variables.stvEditConfig.dataSet.configId == undefined) {
                $scope.Variables.createConfig.setInput('RequestBody', $scope.Variables.stvConfig.dataSet);
                $scope.Variables.createConfig.invoke();
            } else {
                //Update Config
                $scope.Variables.editConfig.setInput('RequestBody', $scope.Variables.stvConfig.dataSet);
                $scope.Variables.editConfig.invoke();
            }

        };

    }
]);