Application.$controller("LocationIntegrationPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        if ($scope.pageParams.locationId === undefined || $scope.pageParams.locationId === "") {
            window.history.back();
        }
    };


    $scope.clientLocationIntegrationTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };


    $scope.clientLocationIntegrationTableDatarender = function($isolateScope, $data) {
        var totalRecords = $scope.Widgets.clientLocationIntegrationTable.dataNavigator.dataSize;
        var maxResults = $scope.Widgets.clientLocationIntegrationTable.dataNavigator.maxResults;
        var currentPage = $scope.Widgets.clientLocationIntegrationTable.dataNavigator.dn.currentPage;

        $scope.paginationPrefix = $scope.$root.getPaginationPrefix(totalRecords, maxResults, currentPage);
    };

}]);


Application.$controller("clientLocationIntegrationTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditLocationIntegration.dataSet = {};
        };

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvEditLocationIntegration.dataSet = $rowData;
            $scope.Variables.stvCreateLocationIntegration.dataSet = $rowData;

        };

    }
]);

Application.$controller("locationIntegrationDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.locationIntegrationFormSubmit = function($event, $isolateScope, $formData) {
            // Create Location Integration
            if ($scope.Variables.stvEditLocationIntegration.dataSet.clientLocationIntegrationId === undefined) {
                $scope.Variables.createClientLocationIntegration.setInput('RequestBody', $scope.Variables.stvCreateLocationIntegration.dataSet);
                $scope.Variables.createClientLocationIntegration.invoke();
            } else {
                //Update Location Integration
                $scope.Variables.editClientLocationIntegration.setInput('RequestBody', processlocIntegData($scope.Variables.stvCreateLocationIntegration.dataSet));
                $scope.Variables.editClientLocationIntegration.invoke();
            }


        };


        $scope.saveButtonClick = function($event, $isolateScope) {
            $scope.Widgets.locationIntegrationForm.submit();
        };
        // Method Process Data before insert
        function processlocIntegData(locInteg) {
            if (locInteg.hipClientLocation !== undefined || locInteg.hipClientLocation !== null) {
                delete locInteg.hipClientLocation;
            }
            if (locInteg.hipIntegration !== undefined || locInteg.hipIntegration !== null) {
                delete locInteg.hipIntegration;
            }
            return locInteg;
        }

    }
]);