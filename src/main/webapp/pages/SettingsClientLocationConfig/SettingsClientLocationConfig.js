Application.$controller("SettingsClientLocationConfigPageController", ["$scope", "NavigationService", function($scope, NavigationService) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        //Check for Location
        if ($scope.pageParams.locationId === undefined || $scope.pageParams.locationId === "") {
            window.history.back();
        }
    };



    $scope.clientLocationConfigListTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;

    };


    $scope.breadcrumbBeforenavigate = function($isolateScope, $item) {
        NavigationService.goToPage($item.id, {
            'urlParams': $scope.pageParams
        });
        return false;
    };


    $scope.clientLocationConfigListTableDatarender = function($isolateScope, $data) {
        var totalRecords = $scope.Widgets.clientLocationConfigListTable.dataNavigator.dataSize;
        var maxResults = $scope.Widgets.clientLocationConfigListTable.dataNavigator.maxResults;
        var currentPage = $scope.Widgets.clientLocationConfigListTable.dataNavigator.dn.currentPage;

        $scope.paginationPrefix = $scope.$root.getPaginationPrefix(totalRecords, maxResults, currentPage);
    };

}]);


Application.$controller("clientLocationConfigListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvCreateLocationConfig.dataSet = $rowData;
            $scope.Variables.stvEditLocationConfig.dataSet = $rowData;
        };


        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditLocationConfig.dataSet = {}
        };

    }
]);

Application.$controller("dialogManageClientLocationController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.createClientLocationConfigFormSubmit = function($event, $isolateScope, $formData) {
            // Create client Config
            if ($scope.Variables.stvEditLocationConfig.dataSet.clientLocationConfigId === undefined) {
                $scope.Variables.createClientLocationConfig.setInput('RequestBody', $scope.Variables.stvCreateLocationConfig.dataSet);
                $scope.Variables.createClientLocationConfig.invoke();
            } else {
                //Update client Config
                $scope.Variables.editClientLocationConfig.setInput('RequestBody', processConfigData($scope.Variables.stvCreateLocationConfig.dataSet));
                $scope.Variables.editClientLocationConfig.invoke();
            }
        };

        $scope.SaveClick = function($event, $isolateScope) {
            $scope.Widgets.createClientLocationConfigForm.submit();
        };

        // Method Process Data before insert
        function processConfigData(config) {
            if (config.hipConfig !== undefined || contact.hipConfig !== null) {
                delete config.hipConfig;
            }
            return config;
        }


    }
]);